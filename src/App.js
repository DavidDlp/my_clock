import './App.css';
import CountDown from './component/CuentaAtras/CuentaAtras';
import DigitalClock from './component/DigitalClock/DigitalClock';
import StopWatch from './component/StopWatch/StopWatch';

function App() {
  return (
    <div>
      <DigitalClock/>
      <hr/>
      <CountDown/>
      <hr/>
      <StopWatch/>
    </div>
  );
}

export default App;
